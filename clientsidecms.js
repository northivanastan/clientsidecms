// @license magnet:?xt=urn:btih:90dc5c0be029de84e523b9b3922520e79e0e6f08&dn=cc0.txt CC0
/*
# ClientSideCMS

This is ClientSideCMS, an attempt to make a set of JS scripts which make it easier to create a website. Simply add it to a page which is in a folder with index.md, footer.md, and navigation.md Markdown files.

You can then style the following IDs:

* \#content
* \#footer
* \#navigation

I aim for ClientSideCMS to be:

* Minimal: Features that are essential, plus perhaps a blog extension, and no more. Only library required so far is Marked (because the amount of code needed to write a markdown parser is staggering).
* Easy-to-use: With the Javascript embedded, creating a set of objects with certain classes or IDs should be all that is needed.
* Client-side: No code will run on the server, making ClientSideCMS usable on [Neocities](https://neocities.org) or anywhere where PHP setup is impossible.
* Well-documented: specifically well-commented.
* LibreJS-friendly

On the other hand, ClientSideCMS is not:
* Compatible with older browsers: ClientSideCMS is designed specifically for new browsers.
    
ClientSideCMS requires [Marked](https://marked.js.org). Make sure to include it before ClientSideCMS in your header.
*/

// URL parameter parser
function urlParameters() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
        vars[key] = value;
    });
    return vars;
}

// Download the Markdown using XMLHttpRequest
function getMD(theUrl) {
    if (window.XMLHttpRequest) {
        // Obtain the Markdown file and return its content.
        xmlhttp = new XMLHttpRequest();
        //FIXME: Remove asynchronous XMLHttpRequest here.
        xmlhttp.open("GET", theUrl, false);
        xmlhttp.send();
        return xmlhttp.responseText;
    } else {
        return "# Browser is unsupported";
    }
}
// Use Marked to parse the Markdown of the main page as soon as the page is loaded.
window.onload = function() {
    if (urlParameters()['p']) {
        //TODO: classes rather than ids
        document.getElementById("content").innerHTML = marked(getMD(urlParameters()['p']));
    } else {
        // Get, parse, and embed content
        document.getElementById("content").innerHTML = marked(getMD("index.md"));
        // Get, parse, and embed navigation
    }
	document.getElementById("header").innerHTML = marked(getMD("header.md"));
	document.getElementById("footer").innerHTML = marked(getMD("footer.md"));
}
// TODO: Better custom formatting for navigation
// TODO: Multi-level/organized navigation - use classes?
// TODO: Store YAML frontmatter as array.
//license-end