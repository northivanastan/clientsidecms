# ClientSideCMS

This is ClientSideCMS, an attempt to make a set of JS scripts which make it easier to create a website. Simply add it to a page which is in a folder with index.md, footer.md, and navigation.md Markdown files.

You can then style the following IDs:

* \#content
* \#footer
* \#navigation

I aim for ClientSideCMS to be:

* Minimal: Features that are essential, plus perhaps a blog extension, and no more. Only library required so far is Marked (because the amount of code needed to write a markdown parser is staggering).
* Easy-to-use: With the Javascript embedded, creating a set of objects with certain classes or IDs should be all that is needed.
* Client-side: No code will run on the server, making ClientSideCMS usable on [Neocities](https://neocities.org) or anywhere where PHP setup is impossible.
* Well-documented: specifically well-commented.
* LibreJS-friendly

On the other hand, ClientSideCMS is not:
* Compatible with older browsers: ClientSideCMS is designed specifically for new browsers.
    
ClientSideCMS requires [Marked](https://marked.js.org). Make sure to include it before ClientSideCMS in your header.